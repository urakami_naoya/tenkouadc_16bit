/* 
 * File:   adc_16bit.h
 * Author: urakaminaoya
 *
 * Created on March 16, 2018, 12:22 AM
 */
#include "adc_16bit.h"

/**
 * drdy_condition
 * 
 * Wait until DRDY is LOW.
 * To know condition of DRDY by read_drdy function
 * Using in reading function's condition
 * 
 * After converting data, need waiting for 9*1/output rate + 2000*CLOCK time(pipeline delay)
 */

static int drdy_condition(adc16_communication_information_t* adc16_condition) {
    for(int time = 0 ; time < DRDY_TIME ; time++) {
        __delay_ms(20);
        if(!read_drdy(adc16_condition)) {
            return 1;
        }
    }
    return 0;
}

/**
 * read_drdy
 * 
 * To know condition of DRDY from communication register.
 * Using in drdy_condition function
 * 
 */
static uint8_t read_drdy(adc16_communication_information_t* adc16_condition) {
    communication_register(adc16_condition,0x08);
    uint8_t rx_data;
    adc16_condition->adc.q_outpin & 0x08 ? 
        select_slave(adc16_condition->adc.cs_port, adc16_condition->adc.cs_pin, 1) : decoder_select_slave(adc16_condition->adc.q_outpin , 1);
    
    spiRead_mode3(&rx_data,1);
    
    adc16_condition->adc.q_outpin & 0x08 ? 
        setPinHigh(adc16_condition->adc.cs_port, adc16_condition->adc.cs_pin) : decoder_select_pin(7);
    return rx_data & 0x80;
}

/**
 * communication_register
 * 
 * Writing communication register.
 * All communication to the part must start with a write operation to its communication register.
 */
static void communication_register(adc16_communication_information_t* adc16_condition , uint8_t condition) {
    uint8_t register_condition = (condition | adc16_condition->convert_channel.channel);
    adc16_condition->adc.q_outpin & 0x08 ? 
        select_slave(adc16_condition->adc.cs_port, adc16_condition->adc.cs_pin, 1) : decoder_select_slave(adc16_condition->adc.q_outpin , 1);
    
    spiWrite_mode3(&register_condition,1);
    
    adc16_condition->adc.q_outpin & 0x08 ? 
        setPinHigh(adc16_condition->adc.cs_port, adc16_condition->adc.cs_pin) : decoder_select_pin(7);
}

/**
 * converting_adc16
 * 
 * The function to read the converting data from ADC
 * 
 * Note
 * -------
 * The structure is defined in header of ADC.
 * 
 * In the future, we will need error handing.
 * If this function doesn't work by some influence, return -1.
 * So, we can carry out error by using this result. 
 * 
 * Usage example:   Define in any order
 *  ------------------------------------------------------------------
 *  adc16_communication_information <NAME>;
 *      <NAME>.adc.cs_port = &PORT<X>; 
 *      <NAME>.adc.cs_pin = 0 ~ 7;
 *      <NAME>.adc.reset_port = &PORT<X>;
 *      <NAME>.adc.reset_pin = 0 ~ 7;
 *      <NAME>.adc.q_outpin = 0 ~ 7 and 8;
 * 
 *      <NAME>.convert_channel.channel = 0x00, 0x01 and 0x03
 *      <NAME>.convert_channel.gain = 0x00 to 0x07
 *      <NAME>.convert_channel.filter = 0x00 to 0x03
 *  ------------------------------------------------------------------
 *  <X>: A ~ E
 *  <NAME>: Freely name the structure
 *  
 * The detail:
 *  <NAME>.adc.cs_port and <NAME>.adc.cs_pin: Select the slave select pin which is connected on devise.
 *  <NAME>.adc.reset_port and <NAME>.adc.reset_pin: Select the reset pin which is connected on devise.
 *  ※In case using decoder, don't need define it.
 * 
 *  <NAME>.adc.q_outpin: Select CS pin through decoder
 * 
 *  <NAME>.convert_channel.channel: Select a converting channel.
 * 
 *  <NAME>.convert_channel.gain: Select gain as follows.
 *    -Argument : GAIN  --- Argument : GAIN -
 *    |  0x00      1     |   0x04       16  |
 *    |  0x01      2     |   0x05       32  |
 *    |  0x02      4     |   0x06       64  |
 *    |  0x03      8     |   0x07       128 |
 *    ---------------------------------------
 *  A detail information is Page 18 of 44 in AD7706 data sheet.
 * 
 *   <NAME>.convert_channel.filter: Select filter as follows.   
 *    - Argument  :  RATE  -
 *    |  0x00       20  [Hz] |   
 *    |  0x01       25  [Hz] | 
 *    |  0x02       100 [Hz] |   
 *    |  0x03       200 [Hz] |  
 *    ------------------------
 *  A detail information is Page 19 of 44 in AD7706 data sheet.
 * 
 */
int converting_adc16(adc16_communication_information_t* adc16_condition , uint8_t* rx_data) {
    if(adc16_condition->adc.q_outpin >= 0x09) { //Error handing if substitute more 7 in adc16_condition->adc.q_outpin
        return -1;
    }   
    communication_register(adc16_condition , 0x20); //Select clock register
    adc16_condition->adc.q_outpin & 0x08 ? 
        select_slave(adc16_condition->adc.cs_port, adc16_condition->adc.cs_pin, 1) : decoder_select_slave(adc16_condition->adc.q_outpin , 1);   
    spiWrite_mode3(&adc16_condition->convert_channel.filter,1);
    adc16_condition->adc.q_outpin & 0x08 ? setPinHigh(adc16_condition->adc.cs_port, adc16_condition->adc.cs_pin) : decoder_select_pin(7);
   
    communication_register(adc16_condition , 0x10); //Select setup register
    uint8_t register_condition = (0x46 | (adc16_condition->convert_channel.gain << 3));  //self calibration
    adc16_condition->adc.q_outpin & 0x08 ? 
        select_slave(adc16_condition->adc.cs_port, adc16_condition->adc.cs_pin, 1) : decoder_select_slave(adc16_condition->adc.q_outpin , 1);
    spiWrite_mode3(&register_condition,1);
    adc16_condition->adc.q_outpin & 0x08 ? setPinHigh(adc16_condition->adc.cs_port, adc16_condition->adc.cs_pin) : decoder_select_pin(7); 
    
    if(drdy_condition(adc16_condition)) { 
        communication_register(adc16_condition , 0x38); //Select data register
        _delay(10); //To match timing communication with PIC and ADC. Thats why is that it got not match timing during working for long time. 
        adc16_condition->adc.q_outpin & 0x08 ? 
        select_slave(adc16_condition->adc.cs_port, adc16_condition->adc.cs_pin, 1) : decoder_select_slave(adc16_condition->adc.q_outpin , 1);
        spiRead_mode3(rx_data,2);
        adc16_condition->adc.q_outpin & 0x08 ? setPinHigh(adc16_condition->adc.cs_port, adc16_condition->adc.cs_pin) : decoder_select_pin(7);
        return 0;
    }
    return -1;
}

/**
 * conversion_voltage
 * 
 * Convert the binary result of the ADC conversion obtained by converting_adc16 to an
 * actual voltage reading.
 */
float conversion_voltage(uint8_t* data) {
    uint16_t n_LSB = (*data << 8) | (*data + 1);
    
    return 0.00007629 * n_LSB;  // 5/2^16 * n_LSB
}

static void set_register(adc16_communication_information_t* adc16_condition,uint8_t condition) {
    adc16_condition->adc.q_outpin & 0x08 ? 
        select_slave(adc16_condition->adc.cs_port, adc16_condition->adc.cs_pin, 1) : decoder_select_slave(adc16_condition->adc.q_outpin , 1);   
    spiWrite_mode3(&condition,1);
    adc16_condition->adc.q_outpin & 0x08 ? setPinHigh(adc16_condition->adc.cs_port, adc16_condition->adc.cs_pin) : decoder_select_pin(7);
}

static void read_data(adc16_communication_information_t* adc16_condition ,uint8_t byte ,uint8_t* rx_data) {   
        adc16_condition->adc.q_outpin & 0x08 ? 
        select_slave(adc16_condition->adc.cs_port, adc16_condition->adc.cs_pin, 1) : decoder_select_slave(adc16_condition->adc.q_outpin , 1);
        spiRead_mode3(rx_data,byte);
        adc16_condition->adc.q_outpin & 0x08 ? setPinHigh(adc16_condition->adc.cs_port, adc16_condition->adc.cs_pin) : decoder_select_pin(7);
}

static int drdy_conditions(adc16_communication_information_t* adc16_condition) {
    for(int time = 0 ; time < DRDY_TIME ; time++) {
        __delay_ms(20);
        uint8_t rx_data;
        set_register(adc16_condition,0x08);
        read_data(adc16_condition,1,&rx_data);
        if(!(rx_data & 0x80)) {
            return 1;
        }
    }
    return 0;
}

int initialize_adc16(adc16_communication_information_t* adc16_condition) {
    uint8_t select_register = 0x20 | adc16_condition->convert_channel.channel;  //Register value to select clock register 
    set_register(adc16_condition,select_register); //Select clock register
    set_register(adc16_condition,adc16_condition->convert_channel.filter); //Set output rate to clock register
    
    select_register = 0x10 | adc16_condition->convert_channel.channel;  //Register value to select setup register
    set_register(adc16_condition,select_register); //Select setup register
    uint8_t register_content = 0x46 | (adc16_condition->convert_channel.gain << 3); //Register value for gain
    set_register(adc16_condition,register_content); //Set gain to setup register
    __delay_ms(500);
}

int converting_adc16s(adc16_communication_information_t* adc16_condition , uint8_t* rx_data) {
    uint8_t select_register = 0x10 | adc16_condition->convert_channel.channel;  //Register value to select setup register
    set_register(adc16_condition,select_register); //Select setup register
    uint8_t register_content = 0x06 | (adc16_condition->convert_channel.gain << 3);
    set_register(adc16_condition,register_content); 
    
    if(drdy_conditions(adc16_condition)) {
        select_register = 0x38 | adc16_condition->convert_channel.channel; 
        set_register(adc16_condition,select_register);  //Select data register
        _delay(10); //To match timing communication with PIC and ADC. Thats why is that it got not match timing during working for long time. 
        read_data(adc16_condition,2,rx_data);
        return 0;
    }
    return -1;
}
