/* 
 * File:   adc_16bit.h
 * Author: urakaminaoya
 *
 * Created on March 16, 2018, 12:22 AM
 */

// PIC16F877 Configuration Bit Settings

// 'C' source line config statements

// CONFIG
#pragma config FOSC = HS        // Oscillator Selection bits (HS oscillator)
#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled)
#pragma config PWRTE = OFF      // Power-up Timer Enable bit (PWRT disabled)
#pragma config CP = OFF         // FLASH Program Memory Code Protection bits (Code protection off)
#pragma config BOREN = OFF      // Brown-out Reset Enable bit (BOR disabled)
#pragma config LVP = OFF        // Low Voltage In-Circuit Serial Programming Enable bit (RB3 is digital I/O, HV on MCLR must be used for programming)
#pragma config CPD = OFF        // Data EE Memory Code Protection (Code Protection off)
#pragma config WRT = OFF        // FLASH Program Memory Write Enable (Unprotected program memory may not be written to by EECON control)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include <xc.h>
#include <stdlib.h>
#include <stdint.h> //To use uint_8, etc.
#include "TenkouGPIO.h"
#include "adc_16bit.h"
#include "uart.h"
#include "spi_master.h"
#include "spi_pins.h"
#include "TK-02-02-01_IF_PV.h"

int test_adc_magnet(void){
    SPI_MOSI_MASTER_INIT = 0;
    SPI_MISO_MASTER_INIT = 1;
    SPI_CK_MASTER_INIT = 0;
    TRISD5 = 0; //CS
    
    //TRISD6 = 0; //For reset adc2
    //TRISD7 = 0; //for cs adc2
    
    uint8_t rx_data[2];
    uint8_t ch1[4] = {'C','H','1',':'};
    uint8_t ch2[4] = {'C','H','2',':'};
    uint8_t ch3[4] = {'C','H','3',':'};
    uint8_t adc1[5] = {'a','d','c','1','_'};
    uint8_t adc2[5] = {'a','d','c','2','_'};
    uint8_t brank[5] = {'b','r','a','n','k'};
    unsigned char cr = 0x0D;
    
    float converting_data[1];
    int status;
    char* buf;
    
    uartInit(9600);
    
    adc16_communication_information_t  trans_adc_1 , trans_adc_2 , adc_1[3] , adc_2[3];
        trans_adc_1.adc.cs_pin = 5; // MAGADC1 CS
        trans_adc_1.adc.cs_port = &PORTD;
        trans_adc_1.adc.q_outpin = 8;
        trans_adc_1.adc.reset_pin = 0;
        trans_adc_1.adc.reset_port = 0;
        
        trans_adc_2.adc.cs_pin = 0;
        trans_adc_2.adc.cs_port = 0;
        trans_adc_2.adc.q_outpin = 4; // MAGADC2 CS
        trans_adc_2.adc.reset_pin = 0;
        trans_adc_2.adc.reset_port = 0;
        
//        trans_adc_1.convert_channel.channel = 0x01;
//        trans_adc_1.convert_channel.filter = 0x00;
//        trans_adc_1.convert_channel.gain = 0x00;
//
//        trans_adc_2.convert_channel.channel = 0x01;
//        trans_adc_2.convert_channel.filter = 0x00;
//        trans_adc_2.convert_channel.gain = 0x00;
        
        for(int time = 0 ; time < 3 ; time++) {
            adc_1[time] = trans_adc_1;
            adc_2[time] = trans_adc_2;
        }
        
        adc_1[0].convert_channel.channel = 0x00;    //CH1
        adc_1[0].convert_channel.filter = 0x00;
        adc_1[0].convert_channel.gain = 0x00;
        
        adc_1[1].convert_channel.channel = 0x01;    //ch2
        adc_1[1].convert_channel.filter = 0x00;
        adc_1[1].convert_channel.gain = 0x00;
                
        adc_1[2].convert_channel.channel = 0x03;    //ch3
        adc_1[2].convert_channel.filter = 0x00;
        adc_1[2].convert_channel.gain = 0x00;
        
        adc_2[0].convert_channel.channel = 0x00;    //CH1 for ADC2
        adc_2[0].convert_channel.filter = 0x00;
        adc_2[0].convert_channel.gain = 0x00;
        
        adc_2[1].convert_channel.channel = 0x01;
        adc_2[1].convert_channel.filter = 0x00;
        adc_2[1].convert_channel.gain = 0x00;
        
        adc_2[2].convert_channel.channel = 0x03;
        adc_2[2].convert_channel.filter = 0x00;
        adc_2[2].convert_channel.gain = 0x00;
        
    decoder_initialize();
    
    decoder_select_pin(7);
    RD5 = 1; //CS(for magnet) is High
    
    
    
    for(;;) {
       // Sending converting data from ADC1 CH1
        converting_adc16(&adc_1[0], rx_data);
        
        converting_data[0] = conversion_voltage(rx_data);
        
        buf = ftoa(converting_data[0], &status);
        uartWriteBytes(adc1,5);
        uartWriteBytes(ch1,4);
        uartWriteBytes(buf,8);
        uartWrite(&cr);
               
        // Sending converting data from ADC1 CH2
        converting_adc16(&adc_1[1], rx_data);
        
        converting_data[0] = conversion_voltage(rx_data);
        
        buf = ftoa(converting_data[0], &status);
        uartWriteBytes(adc1,5);
        uartWriteBytes(ch2,4);
        uartWriteBytes(buf,8);
        uartWrite(&cr);
        
        // Sending converting data from ADC1 CH3
        converting_adc16(&adc_1[2], rx_data);
        
        converting_data[0] = conversion_voltage(rx_data);
        
        buf = ftoa(converting_data[0], &status);
        uartWriteBytes(adc1,5);
        uartWriteBytes(ch3,4);
        uartWriteBytes(buf,8);
        uartWrite(&cr);
       
        
        // Sending converting data from ADC2 CH1
        converting_adc16(&adc_2[0], rx_data);
        
        converting_data[0] = conversion_voltage(rx_data);
        
        buf = ftoa(converting_data[0], &status);
        uartWriteBytes(adc2,5);
        uartWriteBytes(ch1,4);
        uartWriteBytes(buf,8);
        uartWrite(&cr);
        
        // Sending converting data from ADC2 CH2
        converting_adc16(&adc_2[1], rx_data);
        
        converting_data[0] = conversion_voltage(rx_data);
        
        buf = ftoa(converting_data[0], &status);
        uartWriteBytes(adc2,5);
        uartWriteBytes(ch2,4);
        uartWriteBytes(buf,8);
        uartWrite(&cr);
        

        
        // Sending converting data from ADC2 CH3
        converting_adc16(&adc_2[2], rx_data);
        
        converting_data[0] = conversion_voltage(rx_data);
        
        buf = ftoa(converting_data[0], &status);
        uartWriteBytes(adc2,5);
        uartWriteBytes(ch3,4);
        uartWriteBytes(buf,8);
        uartWrite(&cr);
        
        uartWriteBytes(brank,5);
        uartWrite(&cr);
        
        
        uartWriteBytes(brank,5);
        uartWrite(&cr);
    }  
}

int test_adc_16bit(void) {
    uint8_t rx_data[2];
    uint8_t ch1[] = {"CH1:"} , ch2[] = {"CH2:"};
    uint8_t adc1[] = {"ADC1_"};
    uint8_t brank[] = {"BRANK"};
    unsigned char cr = 0x0D;
    TRISD5 = 0; //CS
    TRISD3 = 0; //RESET

    float converting_data[1];
    int status;
    char* buf;

    adc16_communication_information_t  trans_adc_1 , adc_1[3];
        trans_adc_1.adc.cs_pin = 5; // MAGADC1 CS
        trans_adc_1.adc.cs_port = &PORTD;
        trans_adc_1.adc.q_outpin = 8;
        trans_adc_1.adc.reset_pin = 0;
        trans_adc_1.adc.reset_port = 0;

        for(int time = 0 ; time < 3 ; time++) {
            adc_1[time] = trans_adc_1;
        }

        adc_1[0].convert_channel.channel = 0x00;    //CH1
        adc_1[0].convert_channel.filter = 0x00;
        adc_1[0].convert_channel.gain = 0x00;

        adc_1[1].convert_channel.channel = 0x01;    //ch2
        adc_1[1].convert_channel.filter = 0x00;
        adc_1[1].convert_channel.gain = 0x00;

        adc_1[2].convert_channel.channel = 0x03;    //ch3
        adc_1[2].convert_channel.filter = 0x00;
        adc_1[2].convert_channel.gain = 0x00;
        
        RD3 = 0;    //Reset 
        __delay_us(10);
        RD3 = 1;    //Active
        
    for(;;) {
       // Sending converting data from ADC1 CH1
        converting_adc16(&adc_1[0], rx_data);

        converting_data[0] = conversion_voltage(rx_data);

        buf = ftoa(converting_data[0], &status);
        uartWriteBytes(adc1,5);
        uartWriteBytes(ch1,4);
        uartWriteBytes(buf,8);
        uartWrite(&cr);
        
        // Sending converting data from ADC1 CH2
        converting_adc16(&adc_1[1], rx_data);
        
        converting_data[0] = conversion_voltage(rx_data);
        
        buf = ftoa(converting_data[0], &status);
        uartWriteBytes(adc1,5);
        uartWriteBytes(ch2,4);
        uartWriteBytes(buf,8);
        uartWrite(&cr);
        
        uartWriteBytes(brank,5);
        uartWrite(&cr);
        
        
    }
}

int test_faster_adc16(void) {
    uint8_t rx_data[2];
    uint8_t ch1[] = {"CH1:"} , ch2[] = {"CH2:"};
    uint8_t adc1[] = {"ADC1_"};
    uint8_t brank[] = {"BRANK"};
    unsigned char cr = 0x0D;
    TRISD5 = 0; //CS
    TRISD3 = 0; //RESET

    float converting_data[1];
    int status;
    char* buf;
    
    adc16_communication_information_t  trans_adc_1 , adc_1[3];
        trans_adc_1.adc.cs_pin = 5; // MAGADC1 CS
        trans_adc_1.adc.cs_port = &PORTD;
        trans_adc_1.adc.q_outpin = 8;
        trans_adc_1.adc.reset_pin = 0;
        trans_adc_1.adc.reset_port = 0;

    for(int time = 0 ; time < 3 ; time++) {
        adc_1[time] = trans_adc_1;
    }

    adc_1[0].convert_channel.channel = 0x00;    //CH1
    adc_1[0].convert_channel.filter = 0x00;
    adc_1[0].convert_channel.gain = 0x00;

    adc_1[1].convert_channel.channel = 0x01;    //ch2
    adc_1[1].convert_channel.filter = 0x00;
    adc_1[1].convert_channel.gain = 0x00;

    adc_1[2].convert_channel.channel = 0x03;    //ch3
    adc_1[2].convert_channel.filter = 0x00;
    adc_1[2].convert_channel.gain = 0x00;

    RD3 = 0;    //Reset 
    __delay_us(10);
    RD3 = 1;    //Active
    
    
    initialize_adc16(&adc_1[0]);

    for(;;) {
        // Sending converting data from ADC1 CH1
        converting_adc16s(&adc_1[0], rx_data);

        converting_data[0] = conversion_voltage(rx_data);

        buf = ftoa(converting_data[0], &status);
        uartWriteBytes(adc1,5);
        uartWriteBytes(ch1,4);
        uartWriteBytes(buf,8);
        uartWrite(&cr);
        
        uartWriteBytes(brank,5);
        uartWrite(&cr);

    }
}

int main(void){
    uartInit(9600);
    spiMasterInit();
    //int a = test_adc_magnet();  //With decoder program , and two ADC.
    //int d = test_adc_16bit();
    int e = test_faster_adc16();
    
}