/* 
 * File:   adc_16bit.h
 * Author: urakaminaoya
 *
 * Created on March 16, 2018, 12:22 AM
 */

#ifndef ADC_16BIT_H
#define	ADC_16BIT_H

//Definitions cycling time for DRDY 
#define DRDY_TIME 100

#include "decoder.h"
#include "spi_pins.h"
#include "spi_mode_3.h"


typedef struct select_adc16_information {
    uint8_t q_outpin;   //Select CS pin through decoder
    volatile unsigned char *cs_port;    //Slect CS port directly connecting pic to ADC
    unsigned char cs_pin;   //Slect CS port directly connecting pic to ADC
    volatile unsigned char *reset_port; //Slect reset port directly connecting pic to ADC
    unsigned char reset_pin;    //Slect reset pin directly connecting pic to ADC
} select_adc16_information_t;

typedef struct select_converting_channel_information {
    uint8_t channel;    //Select converting channel
    uint8_t gain;   //Define gain each converting channel
    uint8_t filter; //Define filter each converting channel
} select_converting_channel_information_t;

typedef struct adc16_communication_information {
    select_adc16_information_t adc;
    select_converting_channel_information_t convert_channel;
} adc16_communication_information_t;

/**
 * Functions needed to make converting_adc16
 * */
static uint8_t read_drdy(adc16_communication_information_t*);
static int drdy_condition(adc16_communication_information_t*);
static void communication_register(adc16_communication_information_t* , uint8_t);

/**
 * Functions which it is available for users to use in main_sentence.
 * */
int converting_adc16(adc16_communication_information_t*, uint8_t*);
float conversion_voltage(uint8_t*);

//New functions, etc
typedef void(*REGISTER_POINT)(adc16_communication_information_t*,uint8_t);
typedef void(*READ_POINT)(adc16_communication_information_t* ,uint8_t,uint8_t*);

static void set_register(adc16_communication_information_t*,uint8_t);
int initialize_adc16(adc16_communication_information_t*);
int converting_adc16s(adc16_communication_information_t*, uint8_t*);
static int drdy_conditions(adc16_communication_information_t* adc16_condition);
#endif	/* ADC_16BIT_H */